FROM tensorflow/tensorflow:latest-gpu-py3
MAINTAINER sleepless-se "sleepless.se@gmail.com"

RUN apt update && apt install -y git wget curl zip unzip python-pil python-lxml vim nginx
RUN pip install --upgrade pip && pip install uwsgi flask supervisor

COPY . /app

WORKDIR /app
RUN mv etc/nginx.conf /etc/nginx/nginx.conf
RUN mv etc/uwsgi.ini /etc/uwsgi.ini
RUN mv etc/supervisord.conf /etc/supervisord.conf

RUN pip3 install --upgrade pip && pip3 install -r /app/requirements.txt


# api
RUN git clone https://github.com/tensorflow/models.git

# protoc settings
WORKDIR /app/models/research
RUN sh /app/install_proto.sh
RUN protoc object_detection/protos/*.proto --python_out=.
ENV PYTHONPATH=$PYTHONPATH:/api:/api/models/research/:/api/models/research/slim:/api/models/research/object_detection


EXPOSE 80
CMD ["/usr/local/bin/supervisord"]